package com.tz.uttamis.controller;

import com.tz.uttamis.model.PaymentRequest;
import com.tz.uttamis.model.PaymentResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/API")
public class payment {
    @PostMapping("/payment")
    public ResponseEntity<PaymentResponse> makePayment(@RequestBody PaymentRequest paymentRequest){
       ResponseEntity response = new ResponseEntity<>(new PaymentResponse(),HttpStatus.OK);
        return response;
    }
}

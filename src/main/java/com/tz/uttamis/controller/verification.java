package com.tz.uttamis.controller;

import com.tz.uttamis.model.VerificationRequest;
import com.tz.uttamis.model.VerificationResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class verification {

    @PostMapping("/verification")
    public ResponseEntity<VerificationResponse> verify(@RequestBody VerificationRequest verificationRequest){
        return new ResponseEntity<>(new VerificationResponse(), HttpStatus.OK);
    }
}

package com.tz.uttamis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UttamsiApplication {

	public static void main(String[] args) {
		SpringApplication.run(UttamsiApplication.class, args);
	}

}

package com.tz.uttamis.model;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SMSAlert {
    private double amount;
    private String investorAccount;
    private String investorName;
    private double salesPrice;
    private Long units;
}

package com.tz.uttamis.model;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
public class BankReconciliation {
    private Date transactionDate;
    private double amount;
    private String paymentReference;
    private String MSISDN;
    private String transactionRef;
    private String paymentDesc;
    private String bankCode;
}

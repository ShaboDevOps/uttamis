package com.tz.uttamis.model;

import lombok.Getter;
import lombok.Setter;

import javax.xml.crypto.Data;
@Getter
@Setter
public class PaymentRequest {
    private String payerName;
    private double amount;
    private String amountType;
    private String currency;
    private String paymentReference;
    private String paymentType;
    private String paymentDesc;
    private String payerID;
    private String transactionRef;
    private String transactionChannel;
    private Data transactionDate;
    private String token;
    private String checksum;
    private String institutionID;
    private String payerMobile;
}

package com.tz.uttamis.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VerificationData {
    private String payerName;
    private double amount;
    private String amountType;
    private  String paymentReference;
    private String currency;
    private String paymentType;
    private String paymentDesc;
}

package com.tz.uttamis.model;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentResponse {
    private String receipt;
}

package com.tz.uttamis.model;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VerificationResponse {
    private String statusDesc;
    private String token;
    private String checksum;
    private String institutionID;
    private String transactionchannel;
    private VerificationData data;
}

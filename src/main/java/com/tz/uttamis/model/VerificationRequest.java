package com.tz.uttamis.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VerificationRequest {
    private String paymentReference;
    private String token;
    private String checksum;
    private String institutionID;
    private String transactionchannel;
}
